import Link from 'next/link';

// create paths for each slug
export const getStaticPaths = async () => {
  const res = await fetch('https://jsonplaceholder.typicode.com/posts');
  const data = await res.json();
  const paths = data.map((post) => {
    return {
      params: { id: post.id.toString() },
    };
  });
  return {
    paths,
    fallback: false, // to show 404
  };
};

export const getStaticProps = async (context) => {
  const id = context.params.id;
  const [postData, commentData] = await Promise.all([
    fetch('https://jsonplaceholder.typicode.com/posts/' + id).then((res) => res.json()),
    fetch('https://jsonplaceholder.typicode.com/comments/' + id).then((res) => res.json()),
  ]);

  return {
    props: {
      post: postData,
      comment: commentData,
    },
  };
};

const PostDetail = ({ post, comment }) => {
  return (
    <div className='card-details'>
      <Link href='/' className='subtitle'>
        <a>Back to Home</a>
      </Link>
      <section>
        <h1>Title: {post.title}</h1>
      </section>
      <div className='detail-body'>
        <h2 className='subtitle'>Post Details:</h2>
        <div className='mb-6'>{post.body}</div>

        <h2 className='subtitle'>Comments:</h2>
        <div className='comment-body'>
          <div>
            <b>Email: </b>
            {comment.email}
          </div>
          <div>
            <b>Name: </b>
            {comment.name}
          </div>
          <div className='mt-3'>{comment.body}</div>
        </div>
      </div>
    </div>
  );
};

export default PostDetail;
